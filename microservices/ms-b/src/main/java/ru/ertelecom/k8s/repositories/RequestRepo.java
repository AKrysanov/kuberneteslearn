package ru.ertelecom.k8s.repositories;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class RequestRepo
{
    JdbcTemplate jdbcTemplate;

    public RequestRepo(JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate=jdbcTemplate;
    }

    public void InsertDB()
    {
        String sql = "insert into requests(request_date,ms_name) values(now(),'ms-b')";
        jdbcTemplate.update(sql);
    }
}

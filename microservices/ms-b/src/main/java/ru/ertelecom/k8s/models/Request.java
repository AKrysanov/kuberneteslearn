package ru.ertelecom.k8s.models;
import javax.persistence.*;
import java.sql.Date;
import java.util.Set;

@Entity
public class Request {
   @Id
   @Column(name = "request_id")
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Integer id;

   @Column(name="request_date")
   private Date date;

   @Column(name = "ms_name")
   private String name;


}

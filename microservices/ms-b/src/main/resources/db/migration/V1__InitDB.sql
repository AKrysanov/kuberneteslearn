CREATE TABLE IF NOT EXISTS requests
(
    request_id serial NOT NULL,
    request_date timestamp without time zone,
    ms_name character varying COLLATE pg_catalog."default",
    CONSTRAINT requests_pkey PRIMARY KEY (request_id)
)
package ru.ertelecom.k8s.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.ertelecom.k8s.service.ServiceInfo;

import java.util.Map;

@RestController
@RequestMapping("/api/v1")
@Tag(name = "AppController", description = "Контроллер получения информации о приложении")
public class ControllerInfo {

    @Autowired
    ServiceInfo serviceInfo;

    @GetMapping("/ms-a")
    @Operation(summary = "Метод получения информации о приложении", description = "", tags = { "" })
    public Map<String,String> getInfo(){
        return serviceInfo.info();
    }

}
